package com.example.agenda_sqllite;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.agenda_sqllite.database.AgendaContactos;
import com.example.agenda_sqllite.database.AgendaDBHelper;
import com.example.agenda_sqllite.database.Contacto;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private TextView lblRespuesta;
    private Button btnRespuesta;
    private TextView lblNombre;
    private EditText txtid;
    private Button btnBuscar;
    private AgendaContactos db;
    private Button btnAgregar;
  //  private TextView lblNombre;
   // private AgendaContactos db;
    ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    //private CheckBox cbxFavorito;
    private Contacto saveContact;
    private int savedIndex;
    private long indexActual;
    private Contacto savedContact;
    private long id;


    public void limpiar(){
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtNotas.setText("");
        //cbxFavorito.setChecked(false);
        saveContact = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       // lblRespuesta = (TextView) findViewById(R.id.lblRespuesta);
       // btnRespuesta = (Button) findViewById(R.id.btnProbar);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        //txtid = (EditText) findViewById(R.id.txtid);
       // btnBuscar = (Button) findViewById(R.id.btnBuscar);
        db = new AgendaContactos(MainActivity.this);
        btnAgregar = (Button) findViewById(R.id.btnGuardar);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        db = new AgendaContactos(MainActivity.this);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDomicilio);
        edtNotas = (EditText) findViewById(R.id.edtNota);
       // cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnCerrar = (Button) findViewById(R.id.btnCerrar);


        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent (MainActivity.this, ListActivity.class);
                startActivityForResult(i ,0);
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });




/*        btnRespuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Contacto c = new Contacto(1, "Brandon Arias", "12345", "12345", "El conchi", "Hola", 0);
                db.openDataBase();
                long idx = db.insertarContacto(c);
                lblRespuesta.setText("Se agrego contacto con id" + idx);

                db.cerrar();
            }
        });*/

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") ||
                        edtTelefono2.getText().toString().matches("") || edtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                  //  nContacto.setFavorito(cbxFavorito.isChecked() ? 1:0);
                    if (saveContact != null) {

                        nContacto.set_ID(savedIndex);
                        for(int y = 0; y < contactos.size(); y++) {
                            if((int) contactos.get(y).get_ID() == savedIndex) {
                                contactos.set(y,nContacto);
                            }
                        }
                        saveContact = null;
                    } else {
                        long index = indexActual;
                        nContacto.set_ID(index);
                        contactos.add(nContacto);
                        indexActual++;
                    }
                    db.openDataBase();
                    long idx = db.insertarContacto(nContacto);
                    // lblRespuesta.setText("Se agrego contacto con id: " + idx);
                    db.cerrar();
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    limpiar();
                }
            }
        });






    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(Activity.RESULT_OK == resultCode){
            Contacto contacto = (Contacto) data.getSerializableExtra("contacto");
            savedContact = contacto;
            id = contacto.get_ID();
            edtNombre.setText(contacto.getNombre());
            edtTelefono.setText(contacto.getTelefono1());
            edtTelefono2.setText(contacto.getTelefono2());
            edtDireccion.setText(contacto.getDomicilio());
            edtNotas.setText(contacto.getNotas());
            limpiar();
        }
    }
}
