package com.example.agenda_sqllite;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.agenda_sqllite.database.AgendaContactos;
import com.example.agenda_sqllite.database.Contacto;

import java.util.ArrayList;

public class ListActivity extends android.app.ListActivity {
    private TableLayout tblLista;
    ArrayList<Contacto> contactos;
    ArrayList<Contacto> filter;
    private AgendaContactos agendaContactos;
    private Button btnNuevo;
    private MyArrayAdapter adapter;
    private ArrayList<Contacto> listaContacto;
    private AgendaContactos agendaContacto;

    private void llenarLista(){
        agendaContacto.openDataBase();
        listaContacto = agendaContacto.allContactos();
        agendaContacto.cerrar();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        agendaContacto= new AgendaContactos(this);
        llenarLista();

        adapter = new MyArrayAdapter(this, R.layout.layout_contacto, listaContacto);
        String str = adapter.contactos.get(1).getNombre();
        setListAdapter(adapter);
        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        this.filter = this.contactos;

        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnList();
            }
        });
        cargarContactos();
    }

    class MyArrayAdapter extends ArrayAdapter<Contacto>{
        private Context context;
        private int textViewResourceId;
        private ArrayList <Contacto> contactos;

        public  MyArrayAdapter(Context context, int resource, ArrayList<Contacto> contactos){
            super(context, resource);
            this.context = context;
            this.textViewResourceId = resource;
            this.contactos = contactos;
        }



        public View getView(final int position, View converView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId,null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombre);
            TextView lblTelefono = (TextView)view.findViewById(R.id.lblTel1);

            Button btnModificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);

            if(contactos.get(position).getFavorito()>0){
                lblNombre.setTextColor((Color.BLUE));
                lblTelefono.setTextColor((Color.BLUE));
            }
            else{
                lblNombre.setTextColor((Color.BLACK));
                lblTelefono.setTextColor((Color.BLACK));
            }

            lblNombre.setText(contactos.get(position).getNombre());
            lblTelefono.setText(contactos.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    agendaContactos.openDataBase();
                    agendaContactos.deleteContacto(contactos.get(position).get_ID());
                    agendaContactos.cerrar();
                    contactos.remove(position);
                    notifyDataSetChanged();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", contactos.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();

                }
            });
        return view;

        }

    }


    public void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for(int x = 0; x < filter.size(); x++)
        {
            if(filter.get(x).getNombre().contains(s))
                list.add(filter.get(x));
        }

        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }

    private void deleteContacto(long id)
    {
        for(int x = 0; x < filter.size(); x++)
        {
            if(filter.get(x).get_ID() == id)
            {
                Log.e("MSG", "Se elimino");
                this.filter.remove(x);
                break;
            }
        }
        this.contactos = filter;
        tblLista.removeAllViews();
        cargarContactos();
    }
    public View.OnClickListener btnEliminarsAction(final long id)
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ID", String.valueOf(id));
                deleteContacto(id);
            }
        };
    }

    public void cargarContactos() {
        for(int x=0; x < contactos.size(); x++) {
            Contacto c = contactos.get(x);
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
         //   nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accion);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c= (Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent ();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contactos", c);
                    oBundle.putSerializable("list", filter);
                    oBundle.putInt(getString(R.string.action), 1);
                    i.putExtras(oBundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g, c);

            Button btnEliminar = new Button(ListActivity.this);
            btnEliminar.setText("Eliminar");
            btnEliminar.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            btnEliminar.setTextColor(Color.BLACK);
            btnEliminar.setOnClickListener(this.btnEliminarsAction(c.get_ID()));
            btnEliminar.setTag(R.string.contacto_g_index, c.get_ID());
            nRow.addView(btnEliminar);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            this.returnList();
        }
        return super.onKeyDown(keyCode, event);
    }


    private void returnList()
    {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable("list", this.filter);
        bundle.putInt(getString(R.string.action), 0);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
        Log.e("MSG", "Send Data");
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview ,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu); }
}
